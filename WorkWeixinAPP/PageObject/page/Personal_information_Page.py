from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.InformationSettingPage import InformationSettingPage
from WorkWeixinAPP.PageObject.page.basepage import BasePage

"""
此页面为联系人的个人信息页面
在此页面，可以为联系人设置备注、查看所属部门
"""
class Personal_information_Page(BasePage):

    """
    点击右上角的三个点
    其作用是跳转到信息编辑页面
    """
    select_element = (MobileBy.XPATH, "//*[contains(@text, '个人信息')]/../../../../../*[@class='android.widget.LinearLayout'][2]")
    def Select_Edit(self):
        # # 点击有上角三个点
        self.find_and_click(self.select_element)

        #跳转到下一个页面
        return InformationSettingPage(self.driver)