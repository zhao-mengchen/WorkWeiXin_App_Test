from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.Edit_Member_Information_Page import Edit_Member_Information
from WorkWeixinAPP.PageObject.page.basepage import BasePage

"""
点击编辑成员按钮
"""


class InformationSettingPage(BasePage):
    edit_element = (MobileBy.XPATH, "//*[@text='编辑成员']")
    def Edit_Member(self):
        # 点击编辑成员
        self.find_and_click(self.edit_element)

        return Edit_Member_Information(self.driver)
