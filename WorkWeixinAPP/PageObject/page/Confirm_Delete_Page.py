from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage

"""
确定删除页面
"""
class Confirm_Delete_Member(BasePage):
    cinfirm_click = (MobileBy.XPATH, "//*[@text='确定']")
    def confirm_delete_member(self):
        # 等待确认信息出现,并点击确认
        self.webdriver_wait(self.cinfirm_click)

        self.find_and_click(self.cinfirm_click)
        # 局部调用
        from WorkWeixinAPP.PageObject.page.Search_Member_Page import SearchMembersPage

        return SearchMembersPage(self.driver)