"""
通讯录列表页面
"""
from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.addmemberspage import AddMembersPage
from WorkWeixinAPP.PageObject.page.basepage import BasePage
from WorkWeixinAPP.PageObject.page.Search_Member_Page import SearchMembersPage


class ContactListPage(BasePage):
    """
    在该页面会用到搜索功能、添加成员工程能
    因此在这里分别创建“搜索”函数和“添加成员”函数
    """

    addmember_test = "添加成员"
    search_member_test = (MobileBy.ID, "com.tencent.wework:id/hib")

    def add_contact(self):
        """
        添加成员功能
        此页面需要点击“添加成员”按钮，然后跳转到“添加成员详情页”
        :return:
        """
        self.find_scroll(self.addmember_test).click()

        return AddMembersPage(self.driver)

    def search_contact(self):
        """
        搜索功能
        点击搜索图标
        跳转到搜索详情页
        :return:
        """
        self.find_and_click(self.search_member_test)

        return SearchMembersPage(self.driver)
