from time import sleep

from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.Personal_information_Page import Personal_information_Page
from WorkWeixinAPP.PageObject.page.basepage import BasePage

"""
搜索详情页
"""


class SearchMembersPage(BasePage):
    """
    定位输入框，并输入目标信息
    """

    Search_element = (MobileBy.ID, "com.tencent.wework:id/g5b")
    Information_elements = (MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/e3z']/android.widget.TextView")

    def Seatch_First_Information_Number(self, name):
        # 输入联系人姓名
        self.find_and_sendkeys(self.Search_element, name)

        # 睡眠3秒，等待搜索结果加载出来
        sleep(3)

        # 获取当前名字中有相同”name“元素的所有信息，并返回信息数量
        number_first = len(self.finds(self.Information_elements))

        self.back(2)

        return number_first



    def Search_Information(self, name):
        # 输入联系人姓名
        self.find_and_sendkeys(self.Search_element, name)

        # 睡眠3秒，等待搜索结果加载出来
        sleep(3)

        # 获取当前名字中有相同”name“元素的所有信息
        Information_list = self.finds(self.Information_elements)

        # 选择第一个联系人
        Information_list[0].click()
        return Personal_information_Page(self.driver)


    def Validation_Rresults(self, name):
        """
        验证删除是否成功
        :param name:
        :return:
        """

        #等待输入框加载完成
        self.webdriver_wait(self.Search_element)

        #点击输入框，输入搜索信息
        self.find_and_sendkeys(self.Search_element, name)

        #睡眠3秒，等待加载结果出现
        sleep(3)

        #获取搜索结果列表
        end_contacts = self.finds(self.Information_elements)
        result = len(end_contacts)

        self.back()

        return result
