from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage


class Click_in_SuccessfullyPage(BasePage):
    """
    此页面用来检测打卡是否成功
    """
    result_element = (MobileBy.ID, 'com.tencent.wework:id/o_')

    def dk_result(self):
        # 验证是否打卡成功
        return self.find(self.result_element).text

