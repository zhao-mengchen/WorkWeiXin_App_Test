"""
此页面是添加成员详情页
"""
from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage


class AddMembersPage(BasePage):
    # def __init__(self, driver):
    #     self.driver = driver
    add_manully_element = (MobileBy.XPATH, "//*[@text='手动输入添加']")
    toast_wait_element = (MobileBy.XPATH, "//*[@class='android.widget.Toast']")

    def add_manully(self):
        """
        该函数作用是点击“手动输入添加”按钮
        然后跳转到“添加成员信息填写”详情页
        :return:
        """
        # 点击手动输入添加成员
        self.find_and_click(self.add_manully_element)

        #局部导入
        from WorkWeixinAPP.PageObject.page.contactaddpage import ContactAddPage
        return ContactAddPage(self.driver)

    #由于在信息详情页输入联系人信息后，会跳转到此页面，并出现一个toast提示，因此我们可以次处验证是否添加成功
    def get_toast(self):
        # 验证Toast
        # 由于每次添加一个成员，都会有一个Toast弹框，验证添加成员成功
        # 因此我们可以通过这个Toast弹框来确定成员添加成功
        element = self.webdriver_wait(self.toast_wait_element)

        result = element.text
        return result