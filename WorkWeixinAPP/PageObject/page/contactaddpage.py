"""
此页面是点击“手动添加成员”按钮后，跳转到的“添加成员”页面
"""
from appium.webdriver.common.mobileby import MobileBy

from WorkWeixinAPP.PageObject.page.basepage import BasePage


class ContactAddPage(BasePage):
    name_element = (MobileBy.XPATH, "//*[contains(@text, '姓名')]/../*[@class='android.widget.EditText']")
    gender_element = (MobileBy.XPATH, "//*[contains(@text, '性别')]/../*[@class='android.widget.RelativeLayout']")
    gender_element_wait = (MobileBy.XPATH, "//*[@resource-id='com.tencent.wework:id/bem']/*[@clickable='true'][1]")
    male_gender = (MobileBy.XPATH, "//*[@text='男']")
    female_gender = (MobileBy.XPATH, "//*[@text='女']")
    telephone_element = (MobileBy.ID, "com.tencent.wework:id/f8g")
    save_element = (MobileBy.ID, 'com.tencent.wework:id/hi9')

    def set_name(self, name):
        # 添加成员姓名
        # 可能是由于开发人员疏忽，在text属性值“姓名”后边多敲了一个空格
        # 因此在这里需要使用方法“contains”来找到text属性中，包含关键字“姓名”的text元素
        # 然后通过“姓名”定位该输入框的父元素，通过父元素定位输入框
        self.find_and_sendkeys(self.name_element, name)
        return self

    def set_gender(self, gender):
        # 选择性别
        # 通过兄弟节点定位性别选择框
        self.find_and_click(self.gender_element)

        # 等待元素可点击
        self.webdriver_wait(self.gender_element_wait)

        if gender == "男":
            self.find_and_click(self.male_gender)

        elif gender == "女":
            self.find_and_click(self.female_gender)
        return self

    def set_telephone(self, telephone):
        self.find_and_sendkeys(self.telephone_element, telephone)
        return self

    def click_save(self):
        self.find_and_click(self.save_element)
        from WorkWeixinAPP.PageObject.page.addmemberspage import AddMembersPage
        return AddMembersPage(self.driver)
